#pragma once

#include <types.h>
#include <radix.h>
#include <core/class.h>
#include <core/kobject.h>
#include <list.h>

typedef enum
{
	VMO_ANONYM = 0,		/* lazy allocation */
	VMO_DATA = 1,		/* immediate allocation */
	VMO_FILE = 2,		/* file backed */
	VMO_SHM = 3,		/* shared memory */
	VMO_USER_PAGER = 4, /* support user pager */
	VMO_DEVICE = 5,		/* memory mapped device registers */
} vmo_type_t;

class(vmobject_t, kobject_t)
{
	struct radix *radix; /* record physical pages */
	struct list_head vmr_list;
	paddr_t start;
	size_t size;
	vmo_type_t type;
};

typedef struct vmspace_t vmspace_t;

vmobject_t *vmo_create(u64_t size, u64_t type);
void vmo_init(vmobject_t *vmo, vmo_type_t type, size_t len, paddr_t paddr);
void *vmo_map_on_vmspace(vmobject_t *vmo, vmspace_t *vmspace, u64_t addr, u64_t prot, u64_t flags);
void *vmo_map(vmobject_t *vmo, u64_t addr, u64_t prot, u64_t flags);

slot_t sys_vmo_create(u64_t size, u64_t type);
int sys_vmo_write(slot_t slot, u64_t offset, u64_t user_ptr, u64_t len);
int sys_vmo_read(slot_t slot, u64_t offset, u64_t user_ptr, u64_t len);
void *sys_vmo_map(slot_t slot, u64_t addr, u64_t prot, u64_t flags);