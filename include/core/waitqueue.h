#pragma once

#include <list.h>
#include <spinlock.h>

typedef struct wait_queue_entry wait_queue_entry_t;
int autoremove_wake_function(struct wait_queue_entry *wq_entry);

typedef int (*wait_queue_func_t)(struct wait_queue_entry *wq_entry);

struct wait_queue_entry
{
    void *priv;
    wait_queue_func_t func;
    struct list_head entry;
};

typedef struct wait_queue_head
{
    spinlock_t lock;
    struct list_head head;
} wait_queue_head_t;

void init_waitqueue_head(struct wait_queue_head *wq_head);
void init_wait_entry(struct wait_queue_entry *wq_entry);
void init_waitqueue_func_entry(struct wait_queue_entry *wq_entry, wait_queue_func_t func);

void prepare_to_wait_queue(struct wait_queue_head *wq_head, struct wait_queue_entry *wq_entry);
void add_wait_queue(struct wait_queue_head *wq_head, struct wait_queue_entry *wq_entry);
void remove_wait_queue(struct wait_queue_head *wq_head, struct wait_queue_entry *wq_entry);
void wake_up(struct wait_queue_head *wq_head);

typedef struct thread_t thread_t;
thread_t *thread_self(void);
void schedule();

#define __wait_event(wq_head, condition, cmd)          \
    do                                                 \
    {                                                  \
        if (condition)                                 \
            break;                                     \
                                                       \
        struct wait_queue_entry wq_entry;              \
        init_wait_entry(&wq_entry);                    \
        for (;;)                                       \
        {                                              \
            prepare_to_wait_queue(wq_head, &wq_entry); \
            if (condition)                             \
                break;                                 \
            cmd;                                       \
        }                                              \
        remove_wait_queue(wq_head, &wq_entry);         \
    } while (0)

#define wait_event(wq_head, condition) __wait_event(wq_head, condition, schedule())