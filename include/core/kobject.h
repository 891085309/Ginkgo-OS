#pragma once

#include <core/class.h>
#include <core/sys.h>
#include <core/errno.h>
#include <core/time.h>
#include <core/waitqueue.h>

#include <list.h>
#include <atomic.h>

class(kobject_t)
{
    atomic_t refcount;
    uint32_t signals;
    wait_queue_head_t queue;
    uint32_t (*poll)(kobject_t * obj);
};

static void ref(kobject_t *this)
{
    atomic_inc(&this->refcount);
}
static void unref(kobject_t *this)
{
    atomic_dec(&this->refcount);
}

struct kobj_poll_desc_t
{
    slot_t slot;
    uint32_t signals;
    uint32_t rsignals;
};

struct kobj_wait_queue_entry_t
{
    struct wait_queue_entry wq_entry;
    uint32_t signals;
    kobject_t *obj;
    int wait_flag;
};

struct kobj_poll_wqueue_t
{
    thread_t *poll_thread;
    struct kobj_poll_desc_t *desc;
    struct kobj_wait_queue_entry_t *entries;
    bool_t timeout;
};

void object_signal(kobject_t *obj, uint32_t set_mask, uint32_t clear_mask);