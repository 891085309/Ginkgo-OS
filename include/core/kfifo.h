#pragma once

#include <core/class.h>
#include <core/kobject.h>
#include <task.h>
#include <log.h>
#include <fifo.h>

class(kfifo_t, kobject_t)
{
    struct fifo_t *fifo;
};

constructor(kfifo_t, unsigned int size);

#define KFIFO_SIGNAL_WRITE 1

void kfifo_write(kfifo_t *kfifo, unsigned char *buf, unsigned int len);
int kfifo_read(kfifo_t *kfifo, unsigned char *buf, unsigned int len);
void sys_fifo_write(slot_t slot, unsigned char *buf, unsigned int len);
int sys_fifo_read(slot_t slot, unsigned char *buf, unsigned int len);
int sys_fifo_create(unsigned int size);
void sys_fifo_destory(slot_t slot);