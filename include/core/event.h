#pragma once

#include <types.h>

struct input_event_t
{
    unsigned short type;
    unsigned short code;
    unsigned int value;
};

void push_event(struct input_event_t *e);