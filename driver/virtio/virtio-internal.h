#pragma once

#include <types.h>
#include <virtio/virtio_ring.h>
#include <virtio/virtio_input.h>

#define VIRTIO_RING_SIZE (1 << 7)

struct virtio_mmio_desc_t
{
    addr_t addr;
    uint32 device_id;
    int virtio_mmio_bus;
};

struct virtio_queue_t
{
    vring_t queue;
    int idx;
    uint16 ack_used_idx;
};

enum
{
    MMIO_VIRTIO_START = 0x10001000UL,
    MMIO_VIRTIO_END = 0x10008000UL,
    MMIO_VIRTIO_STRIDE = 0x1000UL,
    MMIO_VIRTIO_MAGIC = 0x74726976UL,
    MMIO_VIRTIO_NUM = 8,
};

void virtio_input_config_select(volatile struct virtio_input_config *config, enum virtio_input_config_select select, int subsel);
void virtio_input_config_supported(volatile struct virtio_input_config *config);
void virtio_input_check_if_support(volatile struct virtio_input_config *config,
                                   enum virtio_input_config_select select, int subsel,
                                   uint list[], size_t len);
void virtio_input_request_event(struct virtio_mmio_desc_t *desc, struct virtio_queue_t *data);