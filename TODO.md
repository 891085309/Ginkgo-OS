* [ ] timer
  * [X] implement
  * [ ] syscall
  * [ ] ut
* [ ] framebuffer
  * [ ] syscall
  * [ ] ut
* [ ] thread exit
* [ ] CFS
* [ ] thread, process
* [ ] socket
* [ ] setenv
* [ ] unsetenv
* [ ] getenviron
* [ ] popen
* [ ] pclose
* [ ] fdopen
* [ ] fileno
* [ ] signal
* [ ] getcwd
* [ ] chdir
* [ ] mkdir
* [ ] readdir
* [ ] utimes
* [ ] realpath
* [ ] lstat
* [ ] symlink
* [ ] readlink
* [ ] exec
* [ ] waitpid
* [ ] pipe
* [ ] kill
* [ ] dup
* [ ] dup2
* [ ] open
* [ ] seek
* [ ] read
* [ ] write
* [ ] isatty
* [ ] ttyGetWinSize
* [ ] ttySetRaw
* [ ] remove
* [ ] rename
* [ ] unistd.h
* [ ] sys/time.h
* [ ] dirent.h
* [ ] dlfcn.h
* [ ] termios.h
* [ ] sys/ioctl.h
* [ ] sys/wait.h
* [ ] errno values
* [ ] struct stat
* [ ] support pthread
* [ ] support shared library
* [ ] support ELF
* [ ] vfs:vmo优化速度
* [ ] vfs:fd支持IO多路复用
* [ ] malloc:自动扩展内存
* [ ] debug:backtrace
* [ ] vfs:支持相对路径

