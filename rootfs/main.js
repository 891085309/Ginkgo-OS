import Typr from "/Typr.js";
import * as std from "std";
import * as os from "os";

function TestTypr() {
    let f = std.open("/Droid-Sans-Fallback.ttf", "rb")
    f.seek(0, std.SEEK_END);
    let size = f.tell();
    f.seek(0, std.SEEK_SET);
    let buf = new Uint8Array(size);
    let ret = f.read(buf.buffer, 0, size);

    console.log(`file size: ${size}`)
    console.log(`ret: ${ret}`)

    let font = Typr.parse(buf.buffer)[0]
    let shape = Typr.U.shape(font, "银杏操作系统")
    console.log(JSON.stringify(shape))

    let width = 0

    for (let item of shape) {
        width += item.ax
    }

    let path = Typr.U.shapeToPath(font, shape);
    // let path = Typr.U.glyphToPath(font, 7)
    let svg = Typr.U.pathToSVG(path)
    console.log(svg)

    os.spawn(["/bin/font", svg, width, font.head.unitsPerEm])

}

TestTypr()