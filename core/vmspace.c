#include <core/vmspace.h>
#include <core/vmo.h>
#include <string.h>
#include <vm.h>
#include <radix.h>
#include <malloc.h>
#include <list.h>
#include <log.h>
#include <assert.h>

#define USER_START 0x20000000UL
#define USER_END (0x80000000UL - 1UL)

class_impl(vmspace_t, kobject_t){

};

constructor(vmspace_t)
{
	init_list_head(&this->user_vmr_list);
	/* alloc the root page table page */
	this->pgtbl = alloc_page(1);
	memset((void *)this->pgtbl, 0, PAGE_SIZE);
	map_kernel_page(this->pgtbl);
	/* architecture dependent initilization */
	this->user_current_heap = 0;
	this->user_start = USER_START;
	this->user_end = USER_END; //
}

/* release the resource when a process exits */
int vmspace_destroy(vmspace_t *vmspace)
{
	return 0;
}

static struct vmregion *vmregion_alloc(void)
{
	struct vmregion *vmr = calloc(1, sizeof(struct vmregion));
	init_list_head(&vmr->vmo_node);
	init_list_head(&vmr->node);
	return vmr;
}

static void vmregion_free(struct vmregion *vmr)
{
	list_del_init(&vmr->vmo_node);
	list_del_init(&vmr->node);
	free((void *)vmr);
}

static inline bool_t check_addr_intersect(vaddr_t start1, vaddr_t end1, vaddr_t start2, vaddr_t end2)
{
	if (start1 > end2 || end1 < start2)
		return FALSE;
	return TRUE;
}

static bool_t check_vmr_intersect(vmspace_t *vmspace, struct vmregion *vmr)
{
	vaddr_t new_start = vmr->start;
	vaddr_t new_end = new_start + vmr->size - 1;

	struct vmregion *iter;
	list_for_each_entry(iter, &vmspace->user_vmr_list, node)
	{
		vaddr_t start = iter->start;
		vaddr_t end = start + iter->size - 1;
		if (check_addr_intersect(new_start, new_end, start, end))
			return TRUE;
	}
	return FALSE;
}

static bool_t is_vmr_in_vmspace(vmspace_t *vmspace, struct vmregion *vmr)
{
	struct vmregion *iter;
	list_for_each_entry(iter, &vmspace->user_vmr_list, node)
	{
		if (iter == vmr)
			return TRUE;
	}
	return FALSE;
}

static int add_vmr_to_vmspace(vmspace_t *vmspace, struct vmregion *vmr)
{
	if (check_vmr_intersect(vmspace, vmr))
	{
		LOGE("vmr overlap\n");
		return FALSE;
	}

	vaddr_t new_start = vmr->start;
	vaddr_t new_end = new_start + vmr->size - 1;

	struct vmregion *iter, *n;
	list_for_each_entry_safe(iter, n, &vmspace->user_vmr_list, node)
	{
		vaddr_t start = iter->start;
		vaddr_t end = start + iter->size - 1;
		if (new_end < start)
		{
			list_add_tail(&(vmr->node), &(iter->node));
			return TRUE;
		}
	}

	list_add_tail(&(vmr->node), &(vmspace->user_vmr_list));
	return TRUE;
}

static void del_vmr_from_vmspace(vmspace_t *vmspace, struct vmregion *vmr)
{
	if (!is_vmr_in_vmspace(vmspace, vmr))
		list_del(&(vmr->node));
	vmregion_free(vmr);
}

struct vmregion *find_vmr_for_va(vmspace_t *vmspace, vaddr_t addr)
{
	struct vmregion *vmr;
	vaddr_t start, end;

	list_for_each_entry(vmr, &vmspace->user_vmr_list, node)
	{
		start = vmr->start;
		end = start + vmr->size;
		if (addr >= start && addr < end)
			return vmr;
	}
	return NULL;
}

static int fill_page_table(vmspace_t *vmspace, struct vmregion *vmr)
{
	size_t vm_size;
	paddr_t pa;
	vaddr_t va;
	int ret;

	vm_size = vmr->vmo->size;
	pa = vmr->vmo->start;
	va = vmr->start;

	LOGI("[" $(va) " ~ " $(va + vm_size - 1) "] => [" $(pa) " => " $(pa + vm_size - 1) "]");
	ret = map_range_in_pgtbl(vmspace->pgtbl, va, pa, vm_size, vmr->perm);

	return ret;
}

bool_t arch_vmspace_map_range(vmspace_t *vmspace, vaddr_t va, size_t len, u64_t flags, vmobject_t *vmo)
{
	struct vmregion *vmr;
	int ret;

	va = PGROUNDUP(va);
	if (len < PAGE_SIZE)
		len = PAGE_SIZE;

	vmr = vmregion_alloc();
	if (!vmr)
	{
		ret = FALSE;
		goto out_fail;
	}
	vmr->start = va;
	vmr->size = len;
	vmr->perm = flags;
	vmr->vmo = vmo;
	vmr->vmspace = vmspace;

	ret = add_vmr_to_vmspace(vmspace, vmr);

	if (ret < 0)
		goto out_free_vmr;

	/* on-demand mapping for anonymous mapping */
	if (vmo->type == VMO_DATA)
		fill_page_table(vmspace, vmr);

	list_add_tail(&vmr->vmo_node, &vmo->vmr_list);

	return TRUE;
out_free_vmr:
	vmregion_free(vmr);
out_fail:
	return ret;
}

bool_t vmspace_map_range_user(vmspace_t *vmspace, vaddr_t va, size_t len, u64_t prot, u64_t flags, vmobject_t *vmo)
{
	assert(vmspace && len > 0);

	vaddr_t start = va;
	vaddr_t end = va + len - 1;

	if (start > vmspace->user_end || end < vmspace->user_start)
	{
		PANIC("address space [" $(start) " ~ " $(end) "] is not in user space [" $(vmspace->user_start) " ~ " $(vmspace->user_end) "]");
		return FALSE;
	}

	u64_t arch_flags = 0;

	if (prot | PROT_EXEC)
		arch_flags |= PTE_X;
	if (prot | PROT_WRITE)
		arch_flags |= PTE_W;
	if (prot | PROT_READ)
		arch_flags |= PTE_R;
	return arch_vmspace_map_range(vmspace, va, len, arch_flags | PTE_U, vmo);
}

// bool_t vmspace_map_range_kernel(vmspace_t *vmspace, vaddr_t va, size_t len, u64_t prot, u64_t flags, vmobject_t *vmo)
// {
// 	assert(vmspace && len > 0);

// 	u64_t arch_flags = 0;

// 	if (prot | PROT_EXEC)
// 		arch_flags |= PTE_X;
// 	if (prot | PROT_WRITE)
// 		arch_flags |= PTE_W;
// 	if (prot | PROT_READ)
// 		arch_flags |= PTE_R;
// 	return arch_vmspace_map_range(vmspace, va, len, arch_flags, vmo);
// }

bool_t vmspace_unmap_range(vmspace_t *vmspace, vaddr_t va, size_t len)
{
	assert(vmspace && len > 0);

	struct vmregion *vmr;
	vaddr_t start;
	size_t size;

	vmr = find_vmr_for_va(vmspace, va);
	if (!vmr)
		return -1;
	start = vmr->start;
	size = vmr->size;

	if ((va != start) && (len != size))
	{
		PANIC("we only support unmap a whole vmregion now.\n");
	}

	unmap_range_in_pgtbl(vmspace->pgtbl, va, len);
	del_vmr_from_vmspace(vmspace, vmr);

	return 0;
}

bool_t vmspace_unmap_vmregion(struct vmregion *vmr)
{
	assert(vmr && vmr->vmspace);

	vmspace_t *vmspace = vmr->vmspace;
	vaddr_t start = vmr->start;
	size_t size = vmr->size;
	LOGI("unmap: [" $(start) " ~ " $(start + size - 1) "]");
	unmap_range_in_pgtbl(vmspace->pgtbl, start, size);
	del_vmr_from_vmspace(vmspace, vmr);
	return 0;
}

vaddr_t vmspace_find_unmaped(vmspace_t *vmspace, size_t len)
{
	assert(vmspace && len > 0);
	struct vmregion *iter;

	if (list_empty(&vmspace->user_vmr_list))
		return NULL;

	iter = list_first_entry(&vmspace->user_vmr_list, struct vmregion, node);

	vaddr_t start = vmspace->user_start;
	vaddr_t end = start + len - 1;

	list_for_each_entry(iter, &vmspace->user_vmr_list, node)
	{
		vaddr_t iter_start = iter->start;
		vaddr_t iter_end = iter_start + iter->size - 1;
		if (!check_addr_intersect(vmspace->user_start, vmspace->user_end, iter_start, iter_end))
		{
			PANIC("address space [" $(iter_start) " ~ " $(iter_end) "] is not in user space [" $(vmspace->user_start) " ~ " $(vmspace->user_end) "]");
		}
		if (!check_addr_intersect(start, end, iter_start, iter_end))
			return start;

		start = iter->start + iter->size;
		end = start + len - 1;
	}

	if (start > vmspace->user_end || end < vmspace->user_start)
		return NULL;

	return start;
}