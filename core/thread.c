#include <task.h>
#include <vm.h>
#include <log.h>
#include <kalloc.h>
#include <malloc.h>
#include <list.h>
#include <spinlock.h>
#include <core/sched.h>
#include <smp.h>
#include <string.h>

class_impl(thread_t, kobject_t){};

constructor(thread_t)
{
    LOGD("new thread:" $(this));
    init_list_head(&this->ready_queue_node);
    init_list_head(&this->mlist);
    init_list_head(&this->node);
    init_list_head(&this->wake_list);
}

destructor(thread_t)
{
    timer_cancel(&this->sleep_timer);
}

int nextpid = 1;

extern pagetable_t kernel_pagetable;

void task_mapstacks(thread_t *thread)
{
    // map_range_in_pgtbl(thread->vmspace->pgtbl, thread->kstack, thread->kstack, thread->sz, PTE_R | PTE_W);
}

static void task_helper()
{
    thread_t *current = thread_self();
    task_func_t func = (task_func_t)current->context.s0;
    func();
    thread_exit(current);
}

thread_t *kthread_create(void (*func)(), u64_t arg)
{
    thread_t *thread = new (thread_t);

    thread->kstack = (vaddr_t)alloc_page(THREAD_SIZE / PAGE_SIZE);
    thread->sz = THREAD_SIZE;
    thread->vmspace = new (vmspace_t);
    LOGE("thread->kstack:" $(thread->kstack));

    thread->thread_info.kernel_sp = thread->kstack + THREAD_SIZE;

    thread->context.ra = (unsigned long)task_helper;
    thread->context.s0 = (unsigned long)func;
    thread->context.sp = thread->thread_info.kernel_sp;

    LOGI("context.sp:" $(thread->context.sp));

    task_mapstacks(thread);
    thread->status = TASK_STATUS_SUSPEND;
    return thread;
}

thread_t *thread_create(process_t *process, void *stack, void *pc, void *arg)
{
    thread_t *thread = new (thread_t);

    thread->kstack = (vaddr_t)alloc_page(THREAD_SIZE / PAGE_SIZE);
    memset(thread->kstack, 0, THREAD_SIZE);
    thread->sz = THREAD_SIZE;
    thread->vmspace = dynamic_cast(vmspace_t)(slot_get(process, VMSPACE_OBJ_ID));
    thread->process = process;
    thread->thread_info.kernel_sp = thread->kstack + THREAD_SIZE;
    LOGE("thread->kstack:" $(thread->kstack));

    struct pt_regs *regs = task_pt_regs(thread);
    regs->sp = (uintptr_t)stack;
    regs->a0 = (uintptr_t)arg;
    regs->sepc = (uintptr_t)pc;
    regs->sstatus = csr_read(sstatus);
    regs->sstatus &= ~(SSTATUS_SPP);
    regs->sstatus &= ~(SSTATUS_SIE); //  we must disable interrupt because ret_from_exception restores the sstatus
    regs->sstatus |= SSTATUS_SPIE | SSTATUS_SPIE | SR_FS;
    LOGE("regs->sp:" $(regs->sp));
    void ret_from_exception();

    thread->context.ra = (unsigned long)ret_from_exception;
    thread->context.sp = (unsigned long)regs;

    LOGI("context.sp:" $(thread->context.sp));

    task_mapstacks(thread);
    thread->status = TASK_STATUS_SUSPEND;
    return thread;
}

slot_t sys_thread_create(slot_t process_slot, void *stack, void *pc, void *arg)
{
    process_t *process = dynamic_cast(process_t)(slot_get(process_self(), process_slot));
    thread_t *thread = thread_create(process, stack, pc, arg);
    slot_t slot = slot_alloc_install(process_self(), thread);
    thread_resume(thread);
    return slot;
}

void sys_thread_exit()
{
    thread_t *thread = thread_self();

    if (thread)
        thread_destroy(thread);
}

void thread_exit(thread_t *thread)
{
    thread_destroy(thread);
}

void thread_destroy(thread_t *thread)
{
    if (thread)
    {
        // TODO: free pagetable, task_t

        thread->status = TASK_STATUS_EXIT;
        schedule();
        return;
    }
    PANIC();
}

void thread_resume(thread_t *thread)
{
    if (thread && (thread->status == TASK_STATUS_SUSPEND))
    {
        thread->status = TASK_STATUS_READY;
        scheduler_enqueue_task(thread);
    }
}

void thread_suspend(thread_t *thread)
{
    thread_t *next;

    if (thread)
    {
        if (thread->status == TASK_STATUS_READY)
        {
            thread->status = TASK_STATUS_SUSPEND;
            scheduler_dequeue_task(thread);
        }
        else if (thread->status == TASK_STATUS_RUNNING)
        {
            thread->status = TASK_STATUS_SUSPEND;
            schedule();
        }
    }
}

void sys_yield()
{
    thread_suspend(thread_self());
}

void thread_need_sched(thread_t *thread)
{
    thread->thread_info.flags |= NEED_RESCHED;
}

void thread_clear_sched_flag(thread_t *thread)
{
    thread->thread_info.flags &= ~NEED_RESCHED;
}

// should be kept at the end of the file to let the compiler check for incorrect usage
static thread_t *current = NULL;
static thread_t *idle = NULL;

thread_t *thread_self()
{
    return current;
}

thread_t *thread_self_set(thread_t *thread)
{
    return current = thread;
}

thread_t *get_idle_thread()
{
    return idle;
}

void do_task_init()
{
    idle = new (thread_t);
    idle->vmspace = new (vmspace_t);
    idle->name = "idle";
    idle->status = TASK_STATUS_RUNNING;
    idle->kstack = stack0[smp_processor_id()];
    idle->thread_info.kernel_sp = idle->kstack + THREAD_SIZE;
    thread_self_set(idle);

    asm volatile("mv tp, %0" ::"r"(idle));
}