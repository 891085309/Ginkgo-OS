#include <core/event.h>
#include <core/class.h>
#include <core/kobject.h>
#include <task.h>
#include <log.h>
#include <core/kfifo.h>

static kfifo_t *input_event = NULL;

void push_event(struct input_event_t *e)
{
    kfifo_write(input_event, (unsigned char *)e, sizeof(struct input_event_t));
}

slot_t sys_event_create()
{
    if (!input_event)
    {
        input_event = new (kfifo_t, sizeof(struct input_event_t) * 128);
    }

    return slot_alloc_install(process_self(), input_event);
}

int sys_event_read(struct input_event_t *e)
{
    return kfifo_read(input_event, (unsigned char *)e, sizeof(struct input_event_t));
}