#include <stddef.h>
#include <vfs.h>
#include <syscall.h>
#include <types.h>
#include <stdio.h>
#include <ipc.h>

void fs_dispatch(ipc_msg_t *ipc_msg);

u64_t block_read(struct block_t *blk, u8_t *buf, u64_t offset, u64_t count)
{
    return usys_block_read(buf, offset, count);
}
u64_t block_write(struct block_t *blk, u8_t *buf, u64_t offset, u64_t count)
{
    return usys_block_write(buf, offset, count);
}

void block_sync(struct block_t *blk)
{
}

extern struct filesystem_t filesystem_cpio;

struct block_t block_dev = {
    .name = "virtio",
};

int main(int argc, char **argv)
{
    block_dev.blksz = usys_block_size();
    block_dev.blkcnt = usys_block_count();

    vfs_mount(&block_dev, &filesystem_cpio, "/", MOUNT_RO);
    ipc_register_named_server("vfs", fs_dispatch);

    while (1)
        usys_yield();

    return 0;
}