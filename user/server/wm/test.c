#include <stddef.h>
#include <stdio.h>
#include <ipc.h>
#include <wm.h>

u64_t wm_dispatch(ipc_msg_t *ipc_msg);
void do_wm_event_loop();

static void ipc_handler(ipc_msg_t *ipc_msg)
{
    s64_t ret = wm_dispatch(ipc_msg);
    ipc_return(ret);
}

static int exit_flag = 0;
int main(int argc, char **argv)
{
    ipc_register_named_server("wm", ipc_handler);
    do_wm_event_loop();
    return 0;
}