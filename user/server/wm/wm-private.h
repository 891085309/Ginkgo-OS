#pragma once

#include <stddef.h>
#include <syscall.h>
#include <types.h>
#include <stdio.h>
#include <malloc.h>
#include <ipc.h>
#include <gato/render.h>
#include <list.h>
#include <event.h>
#include <input-event-codes.h>
#include <wm-types.h>

#define window_t ipc_window_t

typedef struct window_t
{
    struct list_head node;
    struct window_config_t config;
    wid_t id;
    slot_t vmo;
    slot_t event;
    surface_t surface[1];
    color_t bg;
} window_t;

struct window_manager_t
{
    struct list_head win_list;
    struct window_t *win_ids[WID_MAX];
    surface_t base[1];
    color_t bg;
    wid_t cursor;
    region_t update;
    slot_t event;
};

enum WM_RENDER_EVENT_TYPE
{
    WM_UPDATE,
};

struct wm_render_event_t
{
    enum WM_RENDER_EVENT_TYPE type;
    union
    {
        struct
        {
            region_t region;
        } update_event;
    };
};

wid_t wid_alloc(window_t *w);
void wid_free(wid_t id);
window_t *win_get(wid_t id);
void wm_raise(wid_t id);
void wm_send_render_event(struct wm_render_event_t e);