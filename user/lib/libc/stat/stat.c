#include <vfs.h>

int stat(const char *path, struct stat *buf)
{
    struct vfs_stat_t stat;
    int ret = vfs_stat(path, &stat);

    if (ret == 0)
    {
        *buf = (struct stat){
            .st_ino = stat.st_ino,
            .st_size = stat.st_size,
            .st_mode = stat.st_mode,
            .st_dev = stat.st_dev,
            .st_uid = stat.st_uid,
            .st_gid = stat.st_gid,
            .st_ctim.tv_sec = stat.st_ctime,
            .st_ctim.tv_nsec = 0,
            .st_atim.tv_sec = stat.st_atime,
            .st_atim.tv_nsec = 0,
            .st_mtim.tv_sec = stat.st_mtime,
            .st_mtim.tv_nsec = 0,
            .st_nlink = 0,
            .st_rdev = 0,
        };
    }
    return ret;
}
