/*
 * libc/time/localtime.c
 */

#include <time.h>

struct tm *localtime_r(const time_t *t, struct tm *res)
{
	return gmtime_r(t, res);
}
