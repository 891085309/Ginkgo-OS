#include <time.h>
#include <syscall.h>

int nanosleep(const struct timespec *req, struct timespec *rem)
{
    usys_nanosleep(usys_deadline_after(req->tv_sec * 1000000000L + req->tv_nsec));
    return 0;
}