/*
 * libc/time/gmtime.c
 */

#include <time.h>

struct tm *gmtime_r(const time_t *t, struct tm *res)
{
	if (__secs_to_tm(*t, res) < 0)
		return NULL;
	res->tm_isdst = 0;
	res->__tm_gmtoff = 0;
	res->__tm_zone = "UTC";
	return res;
}
