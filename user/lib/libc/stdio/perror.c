#include <stdio.h>
#include <string.h>
#include <errno.h>

void perror(const char *msg)
{
	FILE *f = stderr;
	char *errstr = strerror(errno);
	
	if (msg && *msg) {
		fwrite(msg, strlen(msg), 1, f);
		fputc(':', f);
		fputc(' ', f);
	}
	fwrite(errstr, strlen(errstr), 1, f);
	fputc('\n', f);
}
