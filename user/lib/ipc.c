#include <ipc.h>
#include <syscall.h>
#include <stdio.h>
#include <string.h>
#include <log.h>

#define SERVER_STACK_SIZE 0x4000UL
#define SERVER_BUF_SIZE 0x4000UL
#define CLIENT_BUF_SIZE 0x4000UL
#define MAX_CLIENT 16

ipc_msg_t *ipc_create_msg(ipc_struct_t *icb, u64_t data_len, u64_t slot_count)
{
    ipc_msg_t *ipc_msg;
    int i;

    if (sizeof(*ipc_msg) + data_len + sizeof(slot_t) * slot_count > CLIENT_BUF_SIZE)
    {
        LOGE("ipc_msg: out of range");
        return NULL;
    }

    ipc_msg = (ipc_msg_t *)icb->shared_buf;
    ipc_msg->data_len = data_len;
    ipc_msg->slot_count = slot_count;

    ipc_msg->data_offset = sizeof(*ipc_msg);
    ipc_msg->cap_slots_offset = ipc_msg->data_offset + data_len;
    memset(ipc_get_msg_data(ipc_msg), 0, data_len);
    for (i = 0; i < slot_count; i++)
        ipc_set_msg_slot(ipc_msg, i, -1);

    return ipc_msg;
}

char *ipc_get_msg_data(ipc_msg_t *ipc_msg)
{
    return (char *)ipc_msg + ipc_msg->data_offset;
}

int ipc_clear_msg_data(ipc_msg_t *ipc_msg, u64_t offset, u64_t len)
{
    if (offset + len < offset || offset + len > ipc_msg->data_len)
        return -1;

    memset(ipc_get_msg_data(ipc_msg) + offset, 0, len);
    return 0;
}

int ipc_set_msg_data(ipc_msg_t *ipc_msg, char *data, u64_t offset, u64_t len)
{
    if (offset + len < offset || offset + len > ipc_msg->data_len)
        return -1;

    memcpy(ipc_get_msg_data(ipc_msg) + offset, data, len);
    return 0;
}

static slot_t *ipc_get_msg_cap_ptr(ipc_msg_t *ipc_msg, u64_t slot_index)
{
    return (slot_t *)((char *)ipc_msg + ipc_msg->cap_slots_offset) + slot_index;
}

slot_t ipc_get_msg_slot(ipc_msg_t *ipc_msg, u64_t slot_index)
{
    if (slot_index >= ipc_msg->slot_count)
        return -1;
    return *ipc_get_msg_cap_ptr(ipc_msg, slot_index);
}

int ipc_set_msg_slot(ipc_msg_t *ipc_msg, u64_t slot_index, slot_t slot)
{
    if (slot_index >= ipc_msg->slot_count)
        return -1;
    *ipc_get_msg_cap_ptr(ipc_msg, slot_index) = slot;
    return 0;
}

/* FIXME: currently ipc_msg is not dynamically allocated so that no need to free */
int ipc_destroy_msg(ipc_msg_t *ipc_msg)
{
    return 0;
}

int ipc_register_server(server_handler server_handler)
{
    return usys_register_server((u64_t)server_handler, MAX_CLIENT);
}

int ipc_register_named_server(const char *name, server_handler server_handler)
{
    return usys_register_named_server(name, (u64_t)server_handler, MAX_CLIENT);
}

int ipc_register_client(int server_thread_cap, ipc_struct_t *ipc_struct)
{
    int conn_cap;

    struct ipc_vm_config vm_config = {};

    conn_cap = usys_register_client((u32_t)server_thread_cap, (u64_t)&vm_config, CLIENT_BUF_SIZE);

    if (conn_cap < 0)
        return -1;
    ipc_struct->shared_buf = vm_config.buf_base_addr;
    ipc_struct->shared_buf_len = vm_config.buf_size;
    ipc_struct->conn_cap = conn_cap;

    return 0;
}

int ipc_register_client_by_name(const char *name, ipc_struct_t *ipc_struct)
{
    int conn_cap;

    struct ipc_vm_config vm_config = {0};

    conn_cap = usys_register_client_by_name(name, (u64_t)&vm_config, CLIENT_BUF_SIZE);

    if (conn_cap < 0)
        return -1;

    ipc_struct->shared_buf = vm_config.buf_base_addr;
    ipc_struct->shared_buf_len = vm_config.buf_size;
    ipc_struct->conn_cap = conn_cap;

    return 0;
}

u64_t ipc_call(ipc_struct_t *icb, ipc_msg_t *ipc_msg)
{
    return usys_ipc_call(icb->conn_cap, (u64_t)ipc_msg);
}

void ipc_return(u64_t ret)
{
    usys_ipc_return(ret);
}
