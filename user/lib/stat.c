#include <sys/stat.h>
#include <vfs.h>

int mkdir(const char *__path, mode_t __mode)
{
    return vfs_mkdir(__path, __mode);
}