#include <vfs.h>
#include <log.h>
#include <syscall.h>

int open(const char *pathname, int flags, mode_t mode)
{
    return vfs_open(pathname, flags, mode);
}

int close(int fd)
{
    return vfs_close(fd);
}

ssize_t read(int fd, void *buf, size_t count)
{
    return vfs_read(fd, buf, count);
}

ssize_t write(int fd, const void *buf, size_t count)
{
    return vfs_write(fd, buf, count);
}

off_t lseek(int fd, off_t offset, int whence)
{
    return vfs_lseek(fd, offset, whence);
}

int chdir(const char *path)
{
    PANIC("Unimplemented!!");
    return -1;
}

char *getcwd(char *buf, size_t size)
{
    PANIC("Unimplemented!!");
    return "/";
}

unsigned sleep(unsigned seconds)
{
    usys_nanosleep(usys_deadline_after(seconds * 1000000000L));
    return 0;
}

int isatty(int fd)
{
    PANIC("Unimplemented!!");
    return 0;
}