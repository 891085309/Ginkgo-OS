#pragma once

#include <types.h>

int spawn(slot_t *new_process, const char *restrict path,
          slot_t slots[], int nr,
          char *const argv[restrict],
          char *const envp[restrict]);