#pragma once

#define O_RDONLY (1 << 0)
#define O_WRONLY (1 << 1)
#define O_RDWR (O_RDONLY | O_WRONLY)
#define O_ACCMODE (O_RDWR)

#define O_CREAT (1 << 8)
#define O_EXCL (1 << 9)
#define O_NOCTTY (1 << 10)
#define O_TRUNC (1 << 11)
#define O_APPEND (1 << 12)
#define O_DSYNC (1 << 13)
#define O_NONBLOCK (1 << 14)
#define O_SYNC (1 << 15)

#define PATH_MAX 1024