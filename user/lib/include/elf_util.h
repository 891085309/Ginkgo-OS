#pragma once

#include <elf.h>
#include <types.h>

struct elf_info_t
{
    Elf64_Ehdr ehdr;
    Elf64_Phdr *phdr;
    Elf64_Shdr *shdr;
};

bool_t elf_parse_file(const char *code);
struct elf_info_t *elf_get_info(const char *code);
void elf_free_info(struct elf_info_t *info);