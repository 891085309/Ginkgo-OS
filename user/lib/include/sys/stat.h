#pragma once

#include <types.h>
#include <time.h>

#define S_IFDIR (1 << 16)
#define S_IFCHR (1 << 17)
#define S_IFBLK (1 << 18)
#define S_IFREG (1 << 19)
#define S_IFLNK (1 << 20)
#define S_IFIFO (1 << 21)
#define S_IFSOCK (1 << 22)
#define S_IFMT (S_IFDIR | S_IFCHR | S_IFBLK | S_IFREG | S_IFLNK | S_IFIFO | S_IFSOCK)

#define S_ISDIR(m) ((m)&S_IFDIR)
#define S_ISCHR(m) ((m)&S_IFCHR)
#define S_ISBLK(m) ((m)&S_IFBLK)
#define S_ISREG(m) ((m)&S_IFREG)
#define S_ISLNK(m) ((m)&S_IFLNK)
#define S_ISFIFO(m) ((m)&S_IFIFO)
#define S_ISSOCK(m) ((m)&S_IFSOCK)

#define S_IXOTH (1 << 0)
#define S_IWOTH (1 << 1)
#define S_IROTH (1 << 2)
#define S_IRWXO (S_IROTH | S_IWOTH | S_IXOTH)

#define S_IXGRP (1 << 3)
#define S_IWGRP (1 << 4)
#define S_IRGRP (1 << 5)
#define S_IRWXG (S_IRGRP | S_IWGRP | S_IXGRP)

#define S_IXUSR (1 << 6)
#define S_IWUSR (1 << 7)
#define S_IRUSR (1 << 8)
#define S_IRWXU (S_IRUSR | S_IWUSR | S_IXUSR)

#define S_ISUID 04000 /* Set user ID on execution.  */
#define S_ISGID 02000 /* Set group ID on execution.  */

struct stat
{
	u64_t st_ino;
	s64_t st_size;
	u32_t st_mode;
	u64_t st_nlink;
	u64_t st_dev;
	u32_t st_uid;
	u32_t st_gid;
	u64_t st_rdev;
	struct timespec st_atim;
	struct timespec st_mtim;
	struct timespec st_ctim;
};

int stat(const char *, struct stat *);
int mkdir(const char *__path, mode_t __mode);