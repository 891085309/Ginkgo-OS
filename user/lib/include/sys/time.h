#pragma once

#include <time.h>
#include <sys/select.h>

int gettimeofday(struct timeval *, void *);
int utimes(const char *, const struct timeval[2]);