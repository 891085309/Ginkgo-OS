#pragma once

#include <stddef.h>

#ifndef alloca
#define alloca(size) __builtin_alloca(size)
#endif