#pragma once

#include <types.h>
#include <stddef.h>
#include <gato/color.h>
#include <gato/surface.h>
#include <event.h>
#include <wm-types.h>
#include <syscall.h>

int wm_window_poll_event(window_t *win, wm_event_t *e, kduration_t *timeout);

////// IPC
window_t *wm_window_create(int x, int y, int width, int height, color_t bg);
void wm_window_destory(window_t *win);
bool_t wm_window_config_set(window_t *win, struct window_config_t *config);
void wm_window_event_mask_set(window_t *win, uint64_t event_mask);
bool_t wm_window_config_get(window_t *win, struct window_config_t *config);
bool_t wm_window_show(window_t *win);
bool_t wm_window_hide(window_t *win);
void wm_window_update(window_t *win);
void wm_window_raise(window_t *win);
void wm_window_move(window_t *win, int dx, int dy);

void do_wm_init(void);