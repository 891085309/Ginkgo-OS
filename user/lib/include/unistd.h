#pragma once

#include <types.h>

int open(const char *pathname, int flags, mode_t mode);
int close(int fd);
ssize_t read(int fd, void *buf, size_t count);
ssize_t write(int fd, const void *buf, size_t count);
off_t lseek(int fd, off_t offset, int whence);

int chdir(const char *);
char *getcwd(char *, size_t);
unsigned sleep(unsigned seconds);

int isatty(int);