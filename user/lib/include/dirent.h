#pragma once

#include <types.h>
#include <bits/__d_type.h>

/* This is the data type of directory stream objects.
   The actual structure is opaque to users.  */
typedef struct __dirstream DIR;

struct dirent
{
    ino_t d_ino;             /* Inode number */
    off_t d_off;             /* Not an offset; see below */
    unsigned short d_reclen; /* Length of this record */
    unsigned char d_type;    /* Type of file; not supported
                                by all filesystem types */
    char d_name[256];        /* Null-terminated filename */
};

extern DIR *opendir(const char *__name);
extern int closedir(DIR *__dirp);
extern struct dirent *readdir(DIR *__dirp);