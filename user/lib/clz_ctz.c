#include <log2.h>
#include <string.h>

#if defined(__alpha__) || defined(__ia64__) || defined(__x86_64__) || defined(_WIN64) || defined(__LP64__) || defined(__LLP64__)
#define BITS_PER_LONG 64
#endif

/**
 * __ffs64 - find first set bit in a 64 bit word
 * @word: The 64 bit word
 *
 * On 64 bit arches this is a synomyn for __ffs
 * The result is not defined if no bits are set, so check that @word
 * is non-zero before calling this.
 */
static inline unsigned long __ffs64(u64_t word)
{
#if BITS_PER_LONG == 32
	if (((u32_t)word) == 0UL)
		return __ffs((u32_t)(word >> 32)) + 32;
#elif BITS_PER_LONG != 64
#error BITS_PER_LONG not 32 or 64
#endif
	return __ffs((unsigned long)word);
}

#if BITS_PER_LONG == 64

int __clzdi2(long val)
{
    return 64 - fls64((u64_t)val);
}

int __ctzdi2(long val)
{
    return __ffs64((u64_t)val);
}

#endif