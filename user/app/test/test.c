#include <syscall.h>
#include <string.h>
#include <vfs.h>
#include <ipc.h>
#include <types.h>
#include <stdio.h>
#include <wm.h>
#include <environ.h>
#include <log.h>

void test_vmo()
{
    int slot = usys_vmo_create(1024, VMO_DATA);
    char buf[1024] = "hello vmo\n";
    usys_vmo_write(slot, 0, (u64_t)buf, 1024);
    usys_vmo_read(slot, 0, (u64_t)buf, 1024);
    printf(buf);
}

void test_vmo_map()
{
    int slot = usys_vmo_create(1024, VMO_DATA);
    printf("slot:%d\n", slot);
    char buf[1024] = "hello vmo\n";
    usys_vmo_write(slot, 0, (u64_t)buf, 1024);
    void *addr = usys_vmo_map(slot, NULL, PROT_READ | PROT_WRITE, 0);
    printf("map addr:%p\n", addr);
    printf((const char *)addr);
}

void test_nanosleep()
{
    printf("test_nanosleep:start\n");
    usys_nanosleep(usys_deadline_after(100000000)); // 100ms
    printf("test_nanosleep:end\n");
}

void test_futex()
{
    // TODO: write UT
    int i = 1;
    printf("test_futex:start\n");
    // usys_futex_wait(&i, 1);
    // usys_futex_wake(&i);
    printf("test_futex:end\n");
}

void test_vfs()
{
    printf("test vfs\n");
    int fd = vfs_open("/test.txt", O_RDONLY, S_IRWXU);
    printf("fd:%d\n", fd);

    char buf[50] = {};
    size_t read_size = vfs_read(fd, buf, 50);
    printf("read_size:%d buf:%s\n", read_size, buf);
    printf("test vfs end\n");
}

void test_stdio()
{
    printf("test stdio\n");
    FILE *f = fopen("/test.txt", "r");
    printf("f:%p\n", f);

    char buf[50] = {};
    size_t count = fread(buf, 1, 50, f);
    printf("count:%d buf:%s\n", count, buf);
    printf("test stdio end\n");
}

void test_env()
{
    setenv("NAME", "VALUE", 0);
    setenv("NAME", "VALUE1", 0);
    LOGD("getenv: " $(getenv("NAME")));
    setenv("NAME", "VALUE1", 1);
    LOGD("getenv: " $(getenv("NAME")));
    unsetenv("NAME");
    LOGD("getenv: " $(getenv("NAME")));
    setenv("NAME", "VALUE1", 0);
    putenv("NAME1=VALUE1");
    char **envp = environ;
    for (; *envp; envp++)
    {
        LOGD("envp: " $(*envp));
    }
    clearenv();
    LOGD("clearenv");

    envp = environ;
    for (; envp && *envp; envp++)
    {
        LOGD("envp: " $(*envp));
    }
}

void test_wm()
{
    // do_wm_init();
    // wid_t id = wm_window_create(0, 0, 100, 100, RGB(0xff));
    // wm_window_show(id);
    // wm_window_move(id, 100, 100);
}

int main(int argc, char **argv, char **envp)
{
    printf("argc %d\n", argc);
    printf("argv %p\n", argv);

    for (int i = 0; i < argc; i++)
        printf("argv[%d] %s\n", i, argv[i]);

    for (int i = 0; envp && envp[i]; i++)
        printf("envp[%d] %s\n", i, envp[i]);

    printf("main\n");

    test_vmo();
    test_vmo_map();
    test_nanosleep();
    test_futex();
    test_vfs();
    test_stdio();
    test_env();
    test_wm();
    return 0;
}