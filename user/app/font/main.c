#include <wm.h>
#include <gato/surface.h>
#include <gato/render.h>
#include <gato/svg.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <types.h>
#include <stdlib.h>

static void my_draw_text(surface_t *base, char *path, float vb_w, float vb_h, float size, int x, int y, color_t color)
{
    context_t *ctx = &(context_t){0};
    context_init(ctx, base);
    float scale = size * 16 / (vb_h);

    svg_style_t style = (svg_style_t){
        scale : scale,
        translate_x : x,
        translate_y : y + vb_h * scale,
        mirror_y : 1,
    };
    svg_to(ctx, path, style);
    ctx->render(ctx, x, y, (style_t){
        fill_color : color
    });
    context_exit(ctx);
}

int main(int argc, char **argv)
{
    if (argc != 4)
    {
        printf("Invalid args\n");
        return -1;
    }

    do_wm_init();
    window_t *win = wm_window_create(200, 150, 200, 200, RGB(0xffffff));
    wm_window_event_mask_set(win, WM_ABS_EVENT | WM_MOUSEBUTTONDOWN_EVENT);
    wm_window_show(win);

    float font_size = 2.0;
    float height = 0.0;

    for(int i = 0; i < 5; i++)
    {
        my_draw_text(win->connect.surface, argv[1], atoi(argv[2]), atoi(argv[3]), font_size, 0, height, RGB(0));
        height += font_size * 16;
        font_size *= 0.7;
    }

    return 0;
}