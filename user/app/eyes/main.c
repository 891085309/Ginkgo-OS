#include <vfs.h>

#include <input-event-codes.h>
#include <wm.h>
#include <gato/surface.h>
#include <gato/render.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <malloc.h>
#include <types.h>

#define W 200
#define H 100
#define H2 25.0

static void pupil(surface_t *base, int dx, int dy, int x, int y)
{
    float len = sqrtf(dx * dx + dy * dy);

    if (len > H2)
    {
        dx = dx * H2 / len;
        dy = dy * H2 / len;
    }

    draw_ellipse(base, x + dx, y + dy, 8, 8, (style_t){
        fill_color : ARGB(0xff000000),
    });
}

void eyes(surface_t *base, int win_x, int win_y, int x_root, int y_root)
{
    surface_clear(base, ARGB(0), 0, 0, base->width, base->height);

    draw_ellipse(base, 50, 50, 40, 40, (style_t){
        fill_color : ARGB(0),
        stroke_color : ARGB(0xff000000),
        stroke_width : 10,
    });
    draw_ellipse(base, 150, 50, 40, 40, (style_t){
        fill_color : ARGB(0),
        stroke_color : ARGB(0xff000000),
        stroke_width : 10,
    });

    pupil(base, x_root - (win_x + 50), y_root - (win_y + 50), 50, 50);
    pupil(base, x_root - (win_x + 150), y_root - (win_y + 50), 150, 50);
}

int main(int argc, char **argv)
{
    do_wm_init();
    window_t *win = wm_window_create(100, 0, W, H, ARGB(0));
    wm_window_event_mask_set(win, WM_ROOT_ABS_EVENT);
    wm_window_show(win);

    surface_t *base = surface_alloc(W, H);

    eyes(base, 0, 0, 0, 0);
    surface_cover(win->connect.surface, base, 0, 0);
    wm_window_update(win);

    while (1)
    {
        wm_event_t e;
        wm_window_poll_event(win, &e, NULL);

        if (WM_ROOT_ABS_EVENT == e.type)
        {
            struct window_config_t config;
            wm_window_config_get(win, &config);
            eyes(base, config.x, config.y, e.root_abs_event.x_root, e.root_abs_event.y_root);
            surface_cover(win->connect.surface, base, 0, 0);
            wm_window_update(win);
        }
    }
    return 0;
}